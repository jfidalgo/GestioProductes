<%-- 
    Document   : cercarNom
    Created on : 23-may-2018, 19:36:02
    Author     : Fidalgo
--%>

<%@page import="java.util.List"%>
<%@page import="model.negoci.Producte"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body bgcolor="#FFFF99" >


        <%@ include file="myHeader.html" %>

        <form action="Productes?accio=cercarNom" method="post">            

            <center><b>Insereix el nom a cercar</b></center>
            <br><br>
            <table cellspacing="2" cellpadding="2" border="0" align="center">
                <tr>
                    <td align="right">Nom</td>
                    <td><input type="Text" name="nom_" size="9"></td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><input type="Submit" value="Cercar nom"></td>
                </tr>                

            </table>   
        </form>

        <br />

    <center>


        <table cellspacing="2" cellpadding="2" border="2" align="center">


            <tr>
                <td><b>CODI</b></td>
                <td><B>NOM</b></td>
                <td><b>DESCRIPCIO</b></td>
                <td><b>PREU</b></td>

            </tr>

            <%  List<Producte> productes = (List<Producte>) request.getAttribute("llistatNom");
                if (productes != null) {
                    out.print("Numero de Productes " + productes.size());
                    for (Producte p : productes) {
            %>

            <tr>
                <td><%=p.getCodi()%></td>
                <td><%= p.getNom()%></td>
                <td><%= p.getDescripcio()%></td>
                <td><%= p.getPreu()%></td>
            </tr>  

            <%    }%>

        </table>
        <%    } else {
                out.print("No hi ha cap registre cercat");
            }%>

        <br />

        <br> <a   href='index.jsp'>TORNAR</a><br> </center>
    <p>&nbsp;</p>
    <%@ include file="myFooter.html" %>

</body>
</html>

