<%-- 
    Document   : llistatProductes
    Created on : 23-may-2018, 17:34:48
    Author     : Fidalgo
--%>

<%@page import="model.negoci.Producte"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <center>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <title>JSP Page</title>
        </head>
        <body bgcolor="#FFFF99">
            <%@ include file="myHeader.html" %>
            <h1>LLISTAT:</h1>
            <table cellspacing="2" cellpadding="2" border="2" align="center">
                <tr>
                    <td><b>CODI</b></td>
                    <td><B>NOM</b></td>
                    <td><b>DESCRIPCIO</b></td>
                    <td><b>PREU</b></td>

                </tr>
                <%  List<Producte> productes = (List<Producte>) request.getAttribute("llistat");
                    out.print("Numero de Productes " + productes.size());
                    for (Producte p : productes) {
                %>

                <tr>
                    <td><%=p.getCodi()%></td>
                    <td><%= p.getNom()%></td>
                    <td><%= p.getDescripcio()%></td>
                    <td><%= p.getPreu()%></td>
                </tr>  

                <%    }%>

            </table>

        <center><br> <a href='index.jsp'>TORNAR</a><br></center>
        <p>&nbsp;</p>
        <%@ include file="myFooter.html" %>

    </center>
</body>
</html>
