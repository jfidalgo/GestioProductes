<%-- 
    Document   : eliminar
    Created on : 24-may-2018, 2:56:01
    Author     : Fidalgo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body bgcolor="#FFFF99" >


        <%@ include file="myHeader.html" %>

    <center>

        <form action="Productes?accio=eliminar" method="post">            

            <center><b>Codi del producte a eliminar</b></center>
            <br><br>
            <table cellspacing="2" cellpadding="2" border="0" align="center">
                <tr>
                    <td align="right">Codi</td>
                    <td><input type="Text" name="codi_" size="9"></td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><input type="Submit" value="Eliminar"></td>
                </tr>                

            </table>   

            <br />



            <% String resposta = (String) request.getAttribute("eliminar");%>
            <a ><%=(resposta == null) ? "Introdueix el codi" : resposta%> </a>

        </form>
        <br> <a   href='index.jsp'>TORNAR</a><br>

    </center>

    <p>&nbsp;</p>
    <%@ include file="myFooter.html" %>

</body>
</html>
