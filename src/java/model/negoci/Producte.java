/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.negoci;

/**
 *
 * @author Fidalgo
 */
public class Producte {

    private String codi;
    private String nom;
    private String descripcio;
    private double preu;

    public Producte(String codi, String nom, String descripcio, double preu) {
        this.codi = codi;
        this.nom = nom;
        this.descripcio = descripcio;
        this.preu = preu;
    }

    public Producte() {
    }



    public String getCodi() {
        return codi;
    }

    public void setCodi(String codi) {
        this.codi = codi;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    public double getPreu() {
        return preu;
    }

    public void setPreu(double preu) {
        this.preu = preu;
    }

    @Override
    public String toString() {
        return "Codi=" + codi + ", nom=" + nom + ", descripcio=" + descripcio + ", preu=" + preu;
    }

}
