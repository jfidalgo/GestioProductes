package model.dades;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.logging.*;
import model.negoci.*;

public class DAOProducte {

    private Connection con;

    public DAOProducte(Connection con) {
        this.con = con;
    }

    /*
    public boolean afegir(Persona p) {
        boolean afegit = true;
        Statement st = null;
        String sentencia = "INSERT INTO Persona(NIF,NOM,EDAT)"
                + " VALUES('" + p.getNom() + "','" + p.getNif() + "',"
                + p.getEdat() + ")";
        try {
            st = con.createStatement();
            if (st.executeUpdate(sentencia) == 0) {
                afegit = false;
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            afegit = false;
        }
        return afegit;
    }
     */
    public boolean afegir(Producte p) {
        boolean afegit = true;
        PreparedStatement pt = null;
        String sentencia = "INSERT INTO Productes(Codi,Nom,Descripcio,Preu)"
                + " VALUES(?,?,?,?)";
        try {
            pt = con.prepareStatement(sentencia);
            pt.setString(1, p.getCodi());
            pt.setString(2, p.getNom());
            pt.setString(3, p.getDescripcio());
            pt.setDouble(4, p.getPreu());

            if (pt.executeUpdate() == 0) {
                afegit = false;
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            afegit = false;
        } finally {
            tancarRecurs(pt);
        }

        return afegit;
    }

    public boolean eliminar(String codi) throws SQLException {
        boolean eliminar = true;

        String sentenciaSQL = "DELETE FROM PRODUCTES WHERE codi = ?";
        PreparedStatement preparedStatement = null;

        preparedStatement = con.prepareStatement(sentenciaSQL);
        preparedStatement.setString(1, codi);
        int affectedRows = preparedStatement.executeUpdate();

        if (affectedRows == 0) {
            eliminar = false;
        }
        return eliminar;
    }

    public List<Producte> cercarPerNom(String nomCercar) {

        List<Producte> llistaProductesCercats = new ArrayList<>();

        String consulta = "SELECT * FROM Productes WHERE NOM = '" + nomCercar + "'";
        Statement st;
        ResultSet rs;

        try {
            st = con.createStatement();
            rs = st.executeQuery(consulta);
            while (rs.next()) {
                String codi = rs.getString(1);
                String nom = rs.getString(2);
                String descripcio = rs.getString(3);
                double preu = rs.getDouble(4);
                llistaProductesCercats.add(new Producte(codi, nom, descripcio, preu));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return llistaProductesCercats;
    }

    public List<Producte> cercarTots() {
        String consulta = "SELECT * FROM Productes";
        Statement st;
        ResultSet rs;
        List<Producte> llista = new ArrayList<>();
        try {
            st = con.createStatement();
            rs = st.executeQuery(consulta);
            while (rs.next()) {
                String codi = rs.getString(1);
                String nom = rs.getString(2);
                String descripcio = rs.getString(3);
                double preu = rs.getDouble(4);
                llista.add(new Producte(codi, nom, descripcio, preu));
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return llista;
    }

    public boolean modificar(String codi, String nom, String descripcio, double preu) throws SQLException {

        boolean modificat = true;

        String sentenciaSQL = "UPDATE PRODUCTES SET NOM = ?, DESCRIPCIO = ?, PREU = ? WHERE CODI = ?";
        PreparedStatement preparedStatement = null;

        preparedStatement = con.prepareStatement(sentenciaSQL);
        preparedStatement.setString(1, nom);
        preparedStatement.setString(2, descripcio);
        preparedStatement.setDouble(3, preu);
        preparedStatement.setString(4, codi);
        int affectedRows = preparedStatement.executeUpdate();
        if (affectedRows == 0) {
            modificat = false;
        }
        System.out.println("Record is updated into EMPLEAT table!");

        return modificat;
    }

    public Producte cercarPerCodi(String codiCercar) {

        Producte producte = null;

        String consulta = "SELECT * FROM Productes WHERE CODI = '" + codiCercar + "'";
        Statement st;
        ResultSet rs;

        try {
            st = con.createStatement();
            rs = st.executeQuery(consulta);
            if (rs.next()) {
                String codi = rs.getString(1);
                String nom = rs.getString(2);
                String descripcio = rs.getString(3);
                double preu = rs.getDouble(4);
                producte = new Producte(codi, nom, descripcio, preu);
            }
            rs.close();
            st.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return producte;
    }

    public Producte convertToProducte(ResultSet rs) {
        Producte producte = new Producte();
        try {
            String codi = rs.getString(1);
            producte.setCodi(codi);
            String nom = rs.getString(2);
            producte.setNom(nom);
            String descripcio = rs.getString(3);
            producte.setDescripcio(descripcio);
            double preu = rs.getDouble(4);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return producte;
    }

    private void tancarRecurs(AutoCloseable r) {
        try {
            r.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            Logger.getLogger(DAOProducte.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
