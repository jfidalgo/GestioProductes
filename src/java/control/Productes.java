/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.dades.ConfiguracioConnexio;
import model.dades.DAOProducte;
import model.negoci.Producte;

/**
 *
 * @author Fidalgo
 */
public class Productes extends HttpServlet {

    private ConfiguracioConnexio configCon;
    private DAOProducte daoProduc;
    private Connection con;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        configCon = new ConfiguracioConnexio();
        con = configCon.getConnexio();
        daoProduc = new DAOProducte(con);
        System.out.println("CONNEXIÓ: " + configCon.esOberta());
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {

        String action = request.getParameter("accio");
        switch (action) {
            case "llistar":
                List<Producte> productes = daoProduc.cercarTots();
                request.setAttribute("llistat", productes);
                request.getRequestDispatcher("llistatProductes.jsp").forward(request, response);
                break;
            case "afegir":
                String resposta = afegir(request, response);
                request.setAttribute("afegit", resposta);
                anarAPagina("afegir.jsp", request, response);
                break;
            case "cercarNom":
                request.setAttribute("llistatNom", cercarPerNom(request, response));
                request.getRequestDispatcher("cercarNom.jsp").forward(request, response);
                //anarAPagina("cercarNom.jsp", request, response);
                break;
            case "eliminar":
                //eliminar
                String respostaEliminar = eliminar(request, response);
                request.setAttribute("eliminar", respostaEliminar);
                anarAPagina("eliminar.jsp", request, response);
                break;
            case "modificar":
                request.setAttribute("producte", cercaPerCodi(request, response));
                anarAPagina("modificar.jsp", request, response);
                break;
            case "modificarDades":

                String confirmacio = modificar(request, response);
                String insereix = "Insereix Codi";
                request.setAttribute("insereix", insereix);
                request.setAttribute("respostaMod", confirmacio);
                anarAPagina("modificar.jsp", request, response);
                break;

        }

    }

    public Producte cercaPerCodi(HttpServletRequest req, HttpServletResponse res) {

        String codiCercar;

        Producte producte = new Producte();

        if ((codiCercar = req.getParameter("codi_")) != null) {
            producte = daoProduc.cercarPerCodi(codiCercar);
        }

        return producte;

    }

    public String modificar(HttpServletRequest req, HttpServletResponse res) throws SQLException {

        String codi, nom, descripcio, preuS;
        double preu;

        boolean modificat = false;
        boolean preuNumeric = false;

        String resposta = "Modificat correctament";

        if ((codi = req.getParameter("codi_cercat")) == null || (nom = req.getParameter("nom_")) == null || (descripcio = req.getParameter("descripcio_")) == null || (preuS = req.getParameter("preu_")) == null) {
            resposta = "Dades incomplertes";
        } else {

            try {
                preu = Double.parseDouble(preuS);
                preuNumeric = true;
            } catch (NumberFormatException nfe) {
                preuNumeric = false;
            }

            if (preuNumeric) {
                preu = Double.parseDouble(preuS);
                modificat = daoProduc.modificar(codi, nom, descripcio, preu);

                if (!modificat) {
                    resposta = "No modificat";
                }
            } else {
                resposta = "Camp del preu conte caracters no numerics";
            }

        }
        return resposta;
        //codi_cercat
    }

    public String eliminar(HttpServletRequest req, HttpServletResponse res) throws SQLException {

        boolean eliminar = true;

        String codi;

        String resposta = "Producte eliminat correctament";

        if ((codi = req.getParameter("codi_")) == null) {
            resposta = "Introduir codi";
        } else {

            eliminar = daoProduc.eliminar(codi);

            if (!eliminar) {
                resposta = "No s'ha pogut eliminar";
            }

        }

        return resposta;

    }

    private List<Producte> cercarPerNom(HttpServletRequest req, HttpServletResponse res) {
        List<Producte> productesNom = new ArrayList();
        String nom;
        if ((nom = req.getParameter("nom_")) != null) {
            productesNom = daoProduc.cercarPerNom(nom);
        } else {
            productesNom = null;
        }
        return productesNom;
    }

    private String afegir(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException, SQLException {
        boolean validar = true;
        boolean numeric = false;

        String codi, nom, descripcio, preuS;
        double preu = 0;
        String resposta = "El producte s'ha creat correctament";
        if ((codi = req.getParameter("codi_")) == null || (nom = req.getParameter("nom_")) == null || (descripcio = req.getParameter("descripcio_")) == null || (preuS = req.getParameter("preu_")) == null) {
            resposta = "s'han d'emplenar tots els camps";
            validar = false;
        } else {

            try {
                preu = Double.parseDouble(preuS);
                numeric = true;
            } catch (NumberFormatException nfe) {
                numeric = false;
            }

            if (numeric) {
                validar = daoProduc.afegir(new Producte(codi, nom, descripcio, preu));
                if (!validar) {
                    resposta = "No s'ha afegit el producte";
                }
            } else {
                resposta = "Camp del preu conte caracters no numerics";
            }

        }

        return resposta;
    }

    private void anarAPagina(String pagina, HttpServletRequest req, HttpServletResponse res)
            throws IOException, ServletException {

        RequestDispatcher dispatcher = req.getRequestDispatcher(pagina);
        if (dispatcher != null) {
            dispatcher.forward(req, res);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(Productes.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(Productes.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
